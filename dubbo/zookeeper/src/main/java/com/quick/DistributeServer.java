package com.quick;

import org.apache.zookeeper.*;

import java.io.IOException;

public class DistributeServer {
    private static String connectString = "127.0.0.1:2181";
    //            "hadoop102:2181,hadoop103:2181,hadoop104:2181";//集群模式
    private static int sessionTimeout = 2000;
    private ZooKeeper zkClient = null;
    public static void main(String[] args) throws IOException, KeeperException, InterruptedException {
        DistributeServer server=new DistributeServer();
        //连接zookeeper集群
        server.getConnect();

        //注册节点
        server.regist(args[0]);

        //业务逻辑处理
        server.business();
    }

    private void business() throws InterruptedException {
        Thread.sleep(Long.MAX_VALUE);
    }

    private void regist(String hostname) throws KeeperException, InterruptedException {
        String path = zkClient.create("/servers/entity", hostname.getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL);
        System.out.println(hostname+"is online");
    }

    private void getConnect() throws IOException {
        zkClient=new ZooKeeper(connectString, sessionTimeout, new Watcher(){
            @Override
            public void process(WatchedEvent watchedEvent) {
            }
        });
    }
}
