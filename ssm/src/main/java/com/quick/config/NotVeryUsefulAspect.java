package com.quick.config;

import org.apache.log4j.Logger;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;


@Component
@Aspect//切点和连接点和通知所在的类叫做切面
public class NotVeryUsefulAspect {
    final static Logger logger = Logger.getLogger(NotVeryUsefulAspect.class);
    @Pointcut("execution(* com.quick.service..*.*(..))")//声明一个  @Pointcut切点(连接点)  多个连接点形成一个切点
    private void anyOldTransfer(){}

    //通知  切入时机
    @Before("anyOldTransfer()")//前置通知
    public void advice(){
        logger.info("在声明切点扫描的所有方法之前执行！输出日志到文件！");
        System.out.println("在声明切点扫描的所有方法之前执行！");
    }
/*
 * @Before 前置通知，在方法执行之前执行
 * @After 后置通知，在方法执行之后执行（无论是否发生异常）还不能访问目标方法执行的结果
 * @AfterRunning 返回通知，在方法正常结束后 返回结果之后执行 可以访问方法的返回值
 * @AfterThrowing 异常通知，在方法抛出异常之后
 * @Around 环绕通知，围绕着方法执行
 *
 * 五种通知的常见使用场景
 *
 * 前置通知
 * 通知用户开始执行
 *
 * 环绕通知
 * 控制事务 权限控制
 *
 * 后置通知
 * 记录日志(方法已经成功调用)
 *
 * 异常通知
 * 异常处理 控制事务
 *
 * 最终通知
 * 记录日志(方法已经调用，但不一定成功)
 *
 * 五种通知的执行顺序
 * 1.在目标方法没有抛出异常的情况下
 * 前置通知
 * 环绕通知的调用目标方法之前的代码
 * 目标方法
 * 环绕通知的调用目标方法之后的代码
 * 后置通知
 * 最终通知
 *
 * 2.在目标方法抛出异常的情况下
 * 前置通知
 * 环绕通知的调用目标方法之前的代码
 * 目标方法 抛出异常 异常通知
 * 最终通知
 *
 * 3.如果存在多个切面
 * 多切面执行时，采用了责任链设计模式。
 * 切面的配置顺序决定了切面的执行顺序，多个切面执行的过程，类似于方法调用的过程，在环绕通知的proceed()执行时，去执行下一个切面或如果没有下一个切面执行目标方法;
 */
}
