package com.quick.controller;

import com.quick.pojo.Books;
import com.quick.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Controller
//@RestController
public class BookController {
    @Autowired
    @Qualifier("bookService")//指定注入接口的名称
    private BookService bookService;
    //返回页面方式
    @RequestMapping("/book/allBook")
    public String list(Model model) {
        List<Books> list = bookService.queryAllBook();
        model.addAttribute("list", list);
        return "allbook";
    }
    //返回json方式
    @ResponseBody
    @RequestMapping("/book/list")
    public List<Books> list() {
        return  bookService.queryAllBook();
    }

}
