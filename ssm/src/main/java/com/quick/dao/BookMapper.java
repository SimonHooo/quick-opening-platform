package com.quick.dao;

import com.quick.pojo.Books;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BookMapper {
    //增
    int addBook(Books books);
    //删
    int deleteBookById(@Param("id") int id);
    //改
    int updateBook(Books books);
    //查
    Books queryBookById(@Param("id") int id);
    List<Books> queryAllBook();
}

