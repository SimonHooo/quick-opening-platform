package com.quick.service;

import com.quick.dao.BookMapper;
import com.quick.pojo.Books;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service(value = "bookService")//如果是配置文件注入就不用加该注解了
public class BookServiceImpl implements BookService{
//    使用配置文件注入
//    private BookMapper bookMapper;
//
//    public void setBookMapper(BookMapper bookMapper) {
//        this.bookMapper = bookMapper;
//    }
//    使用注解注入
    @Resource
    private BookMapper bookMapper;

    @Override
    public int addBook(Books books) {
        return bookMapper.addBook(books);
    }

    @Override
    public int deleteBookById(int id) {
        return bookMapper.deleteBookById(id);
    }

    @Override
    public int updateBook(Books books) {
        return bookMapper.updateBook(books);
    }

    @Override
    public Books queryBookById(int id) {
        return bookMapper.queryBookById(id);
    }

    @Override
    public List<Books> queryAllBook() {
        return bookMapper.queryAllBook();
    }
}
