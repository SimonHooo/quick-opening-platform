package example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import example.config.MyGatewayFilter;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class AuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }
    @Bean
    public MyGatewayFilter myGatewayFilter(){
        return new MyGatewayFilter();
    }
}
