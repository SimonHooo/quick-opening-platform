<%--
  Created by IntelliJ IDEA.
  User: hzy
  Date: 2020/1/3
  Time: 18:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>登录</title>
</head>
<link rel="stylesheet" href="css/bootstrap.min.css">
<style type="text/css">
    body {
        background: #5bc0de url("img/bgt.jpg") no-repeat;
        background-size: 100% 100%;
        background-attachment: fixed;
    }

    .formBox {
        width: 500px;
        height: 600px;
        margin: 0 auto;
    }

    .btnDw {
        margin: 0 auto;
        text-align: center;
    }

    .zca {
        color: #ff6700;
        position: relative;
        top: 86px;
        left: 37px;
    }

    .zca :hover {
        color: #fff;
    }

    .box {
        width: 600px;
        height: 550px;
        background-color: rgba(0, 0, 0, 0.07);
        opacity: 0.8;
        margin: 0 auto;
        box-shadow: 7px 6px 18px #ccc;
    }

    .iga {
        background: #212a5b;

        color: #fff;
    }
</style>
<body>
<div id="app">
    <div class="jumbotron newyear">
        <div class="container">
            <h1><b style="color: rgb(169,99,96)">{{name}}</b></h1>
            <p>
                新年、新事、新开始、新起点、定有新的收获，祝朋友们事事如意，岁岁平安，精神愉快，春节好。 新年好！新年到，好事全到了！祝您及全家新年快乐！身体健康！工作顺利！吉祥如意！
                祝新年行大运！仕途步步高升、万事胜意！麻雀得心应手、财源广...
            </p>
            <p><a class="btn btn-primary btn-lg" onclick="window.location.href='index.html'" role="button">进入主页！</a></p>
        </div>
    </div>
    <div class="box">
        <div class="page-header">
            <h1 style="text-align: center;color: #fff;"><b>登录页面</b></h1>
        </div>
        <div class="formBox">
            <div class="input-group">
                <span class="input-group-addon iga" id="basic-addon1">用户名</span>
                <label>
                    <input type="text" maxlength="6" v-model="name" class="form-control" name="username"
                           placeholder="请输入用户名" aria-describedby="basic-addon1">
                </label>
            </div>
            <div class="input-group" style="margin-top: 50px;">
                <span class="input-group-addon iga" id="basic-addon2">密码</span>
                <label>
                    <input type="password" maxlength="12" class="form-control" name="password" placeholder="请输入密码！"
                           aria-describedby="basic-addon2">
                </label>
            </div>
            <div class="btnDw" style="margin-top: 40px;">
                <input type="button" onclick="bs()" class="btn btn-default btn-primary" style="width: 250px;"
                       value="登录">
                <a href="http://www.baidu.com" class="zca">还没有账号?去注册!</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/vue.min.js"></script>
<script type="text/javascript">
    let vm = new Vue({
        el: "#app",
        data: {
            name: ""
        }
    });

    function bs() {
        var name = $("input[name='username']").val();
        var pwd = $("input[name='password']").val();
        if (name !== "" && pwd !== "") {
            $.post("LoginServlet", {"username": name, "password": pwd}).then(function (res) {
                alert(res);
            })
        } else {
            alert("用户名/密码不能为空！");
        }
    }
</script>
</body>
</html>
