package dao;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import entity.UserDO;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class UserDao implements UserMapper {
    private SqlSession session = null;

    public UserDao() throws IOException {
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        InputStream is = Resources.getResourceAsStream("mybatis.xml");
        SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(is);
        session = sqlSessionFactory.openSession();
    }

    @Override
    public int insertUser(UserDO record) {
        int i = session.insert("dao.UserDao.insertUser", record);
        session.commit();
        System.out.println(i);
        return i;
    }

    @Override
    public int oneSelectPhone(String value) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("value", value);
        return session.selectOne("dao.UserDao.oneSelectPhone", map);
    }

    @Override
    public int oneSelectUsername(String value) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("value", value);
        return session.selectOne("dao.UserDao.oneSelectUsername", map);
    }

    public String oneSelectUserPass(String user, String pass) {
        Map<String, String> map = new HashMap<>();
        map.put("user", user);
        map.put("pass", pass);
        Map<String, Object> maps = oneSelectUserPass(map);
        System.out.println(maps);
        if (maps != null) {
            return (String)maps.get("username");
        } else {
            return null;
        }
    }

    @Override
    public Map<String, Object> oneSelectUserPass(Map<String, String> map) {
        return session.selectOne("dao.UserDao.oneSelectUserPass", map);
    }
}
