package dao;

import entity.UserDO;

import java.util.Map;

interface UserMapper {
    int insertUser(UserDO record);
    int oneSelectPhone(String value);
    int oneSelectUsername(String value);
    Map<String, Object> oneSelectUserPass(Map<String, String> map);
}