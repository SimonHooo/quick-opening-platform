package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDO {
    private Integer id;

    private String phone;

    private String username;

    private String password;

    private String createTime;

    public UserDO(String phone, String username, String password, String createTime) {
        this.phone = phone;
        this.username = username;
        this.password = password;
        this.createTime = createTime;
    }
}