package controller;

import dao.UserDao;
import entity.UserDO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet(name = "RegisterServlet", urlPatterns = "/RegisterServlet")
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String phone = request.getParameter("phone");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        UserDao userDao = new UserDao();
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        int checkPhone = userDao.oneSelectPhone(phone);

        int checkUsername = userDao.oneSelectUsername(username);

        PrintWriter out = response.getWriter();

        if (checkPhone == 0) {
            if (checkUsername == 0) {
                int ins = userDao.insertUser(new UserDO(phone, username, password, sdf.format(date)));
                if (ins > 0) {
                    out.write("true");
                } else {
                    out.write("false");
                }
            } else {
                out.write("该名字已被使用！");
            }
        } else {
            out.write("该手机号已被注册！");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        this.doPost(request, response);
    }
}
